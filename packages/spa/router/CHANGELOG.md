# @atlaskit/router

## 0.0.1

### Patch Changes

- [patch][3bd51496d3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3bd51496d3):

  Initial migration of the router package from Jira

- [patch][ff7a1e5828](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ff7a1e5828):

  Migrated changes to resource-store and router-store from jira-frontend
